import serial
import pygame
from math import pi
from time import time
 
# Initialize the game engine
pygame.init()
 
# Define the colors we will use in RGB format
BLACK = (  0,   0,   0)
WHITE = (255, 255, 255)
BLUE =  (  0,   0, 255)
GREEN = (  0, 255,   0)
RED =   (255,   0,   0)

orientation = 0

x = 195
x_c = 0
x_s = 0
y = 195
y_c = 0

gx = 195
gx_s = 0

size = [400, 400]
screen = pygame.display.set_mode(size)
myfont = pygame.font.SysFont("helvetica", 24)

done = False
clock = pygame.time.Clock()

ser = serial.Serial(port='COM9', baudrate=115200)

read = ser.readline()
print read
data = read.split('\t')
ax = int(data[0])
ay = int(data[1])
gz = int(data[5])
x_c = ax * -1
y_c = ay * -1
gz_c = gz * -1
for i in range(10):
    read = ser.readline()
    data = read.split('\t')
    ax = int(data[0])
    ay = int(data[1])
    gz = int(data[5])
    x_c += ax * -1
    x_c /= 2
    y_c += ay * -1
    y_c /= 2
    gz_c += gz
    gz_c /= 2

last_time = time()

while (not done):
    #clock.tick(10)
    for event in pygame.event.get(): # User did something
        if event.type == pygame.QUIT: # If user clicked close
            done=True
    read = ser.readline()
    ser.flushInput()
    ser.flushOutput()
    data = read.split('\t')
    ax = int(data[0])
    ay = int(data[1])
    az = int(data[2])
    gx = int(data[3])
    gy = int(data[4])
    gz = int(data[5])
    x_s += ax + x_c
    gx_s += gx / 100
    time_elapsed = time() - last_time
    last_time = time()
    orientation += ((gz - gz_c) / 32.8) * time_elapsed
    x += round(gx, -2) / 400
    y += (round(gy, -2) + 100) / 400
    #print round(gx, -2), round(gy, -2)
    screen.fill(WHITE)
    pygame.draw.ellipse(screen, RED, [y, x, 10, 10])
    tekst = myfont.render(str(orientation), 1, (0,0,0))
    screen.blit(tekst, (1, 1))
    pygame.display.flip()

pygame.quit()
ser.close()
